'use strict'

userApp.controller('UserListCtrl', function ($scope, UsersService, PostsService) {
  $scope.users = UsersService.query();

  PostsService.query(function (response) {
      $scope.posts = response;
  })
})
