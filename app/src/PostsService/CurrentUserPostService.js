angular
    .module('UserApp')
    .factory('CurrentUserPostsService', function ($resource) {
        return $resource('https://jsonplaceholder.typicode.com/users/:userId/posts', {userId: '@userId'});
    });

/*
* Новый FACTORY для получения списка постов для текущего пользователя
* */